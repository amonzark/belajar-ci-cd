from typing import List

#import click
import yaml
#import sys
#import os
#import json
#import gitlab
import subprocess

def get_modified_entries(hosted_zone: str) -> List[str]:
    manifest_path = f"{hosted_zone}.yaml"

    try:
        commits = (
            subprocess.run(
                ["git", "rev-list", "-n", "1", "HEAD", "--", manifest_path],
                capture_output=True,
                text=True,
                check=True,
            )
            .stdout.strip()
            .split("\n")
        )

        previous_commit = commits[1] if len(commits) > 1 else ""

        previous_manifest = {}
        if previous_commit:
            previous_manifest_raw = subprocess.run(
                ["git", "show", f"{previous_commit}:{manifest_path}"],
                capture_output=True,
                text=True,
                check=True,
            ).stdout
            previous_manifest = yaml.safe_load(previous_manifest_raw)

        with open(manifest_path, "r") as file:
            current_manifest = yaml.safe_load(file)

        changed_entries = [
            record
            for record in current_manifest
            if record not in previous_manifest
            or current_manifest[record] != previous_manifest[record]
        ]

        return changed_entries

    except Exception as e:
        print(f"Error encountered: {e}")
        return []

get_modified_entries(test2)