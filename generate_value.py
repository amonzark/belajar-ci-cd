
import sys

def testing1():
    value = "hasil dari testing 1"
    f = open('value.txt', 'w')
    f.write(value)
    print(value)

def testing2():
    value = "hasil dari testing 2"
    f = open('value.txt', 'w')
    f.write(value)
    print(value)

    def testing3(): 
        value = "hasil dari testing 3"
        f = open('value.txt', 'w')
        f.write(value)
        print(value)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        function_name = sys.argv[1]
        if function_name == "testing1":
            testing1()
        elif function_name == "testing2":
            testing2()
        elif function_name == "testing3":
            testing3()